<?php

use App\Http\Controllers\CiudadController;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\MaterialProyectoController;
use App\Http\Controllers\MedidaController;
use App\Http\Controllers\ProyectoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

function crudApiResource($name, $controller) {
    Route::get($name."/", [$controller, "obtenerTodos"]);
    Route::get($name."/{id}", [$controller, "obtenerPorId"]);
    Route::post($name."/", [$controller, "crearModelo"]);
    Route::put($name."/{id}", [$controller, "actualizarModelo"]);
    Route::delete($name."/{id}", [$controller, "borrarModelo"]);
}

Route::prefix("v1")->group(function() {
    $definitions = [
        "proyectos" => ProyectoController::class,
        "materiales" => MaterialController::class
    ];

    foreach ($definitions as $key => $value) {
        crudApiResource($key, $value);
    }

    Route::get("departamentos", [DepartamentoController::class, "obtenerTodos"]);
    Route::get("departamento/{departamento}/ciudades", [CiudadController::class, "obtenerTodosPorDepartamento"]);

    Route::get("medidas", [MedidaController::class, "obtenerTodos"]);

    Route::get("proyecto/{id}/materiales", [MaterialProyectoController::class, "obtenerMaterialesDeProyecto"]);
    Route::post("proyecto/{id}/materiales", [MaterialProyectoController::class, "agregarMaterialesAProyecto"]);
    Route::put("proyecto/{id}/materiales", [MaterialProyectoController::class, "actualizarMaterialesDeProyecto"]);

    Route::get("proyecto/{id}/materiales/reporte", [MaterialController::class, "obtenerReportePorProyecto"]);
});