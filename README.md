<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## API Proyectos

API para la gestión de materiales en un proyecto, realizado en laravel.

## Instalación

Ejecutar el siguiente comando

- php composer install

Con eso, ya tendremos todas las dependencias del proyecto, y para ejecutar localmente el comando sería

- php artisan serve

## Uso Básico

A través de un cliente para realizar peticiones HTTP, como POSTMAN, se puede acceder a los endpoints disponibles. Estos endpoints pueden conocerse ejecutando el siguiente comando:

- php artisan route:list

Igualmente, la definición de los endpoints puede encontrarse en la siguiente página de la [wiki](https://gitlab.com/NVFox/laravel-proyecto-api/-/wikis/API-Endpoints).

## Wiki

En la sección [wiki](https://gitlab.com/NVFox/laravel-proyecto-api/-/wikis/Wiki-Home-Page) de Gitlab, se puede encontrar más documentos para el uso de esta API.