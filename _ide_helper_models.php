<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Ciudad
 *
 * @property string $id
 * @property string $departamento_id
 * @property string $nombre
 * @property-read \App\Models\Departamento $departamento
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad whereDepartamentoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ciudad whereNombre($value)
 */
	class Ciudad extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Departamento
 *
 * @property string $id
 * @property string $nombre
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Ciudad> $ciudades
 * @property-read int|null $ciudades_count
 * @method static \Illuminate\Database\Eloquent\Builder|Departamento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Departamento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Departamento query()
 * @method static \Illuminate\Database\Eloquent\Builder|Departamento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departamento whereNombre($value)
 */
	class Departamento extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Material
 *
 * @property string $id
 * @property string $codigo
 * @property string $medida_unidad
 * @property int $unidad
 * @property string $divisa
 * @property int $precio
 * @property string $descripcion
 * @property-read \App\Models\Medida $medida
 * @method static \Illuminate\Database\Eloquent\Builder|Material newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Material newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Material query()
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereDivisa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereMedidaUnidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Material whereUnidad($value)
 */
	class Material extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Medida
 *
 * @property string $id
 * @property string $nombre
 * @method static \Illuminate\Database\Eloquent\Builder|Medida newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Medida newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Medida query()
 * @method static \Illuminate\Database\Eloquent\Builder|Medida whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Medida whereNombre($value)
 */
	class Medida extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Proyecto
 *
 * @property string $id
 * @property string $ciudad_id
 * @property string $nombre
 * @property-read \App\Models\Ciudad $ciudad
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Material> $materiales
 * @property-read int|null $materiales_count
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto query()
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto whereCiudadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Proyecto whereNombre($value)
 */
	class Proyecto extends \Eloquent {}
}

