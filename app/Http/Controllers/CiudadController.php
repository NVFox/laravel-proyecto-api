<?php

namespace App\Http\Controllers;

use App\Http\Resources\CiudadResource;
use App\Services\ciudad\ICiudadService;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function __construct(
        protected ICiudadService $ciudadService
    ) {}

    public function obtenerTodosPorDepartamento(string $departamento)
    {
        return CiudadResource::collection($this->ciudadService
            ->encontrarTodosPorDepartamento($departamento));
    }
}
