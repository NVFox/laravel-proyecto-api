<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMaterialRequest;
use App\Http\Requests\UpdateMaterialRequest;
use App\Http\Resources\MaterialResource;
use App\Http\Resources\ReporteMaterialResource;
use App\Services\material\IMaterialService;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function __construct(
        protected IMaterialService $materialService
    ) {}

    public function obtenerReportePorProyecto(string $id) {
        return ReporteMaterialResource::make($this->materialService->obtenerReportePorProyecto($id));
    }

    public function crearModelo(StoreMaterialRequest $request)
    {
        return MaterialResource::make($this->materialService->crearModelo($request->validated()))
            ->response()
                ->setStatusCode(201);
    }

    public function actualizarModelo(string $id, UpdateMaterialRequest $request)
    {
        $this->materialService->actualizarModelo($id, $request->validated());
        return response()->json()
            ->setStatusCode(204);
    }

    public function borrarModelo(string $id) 
    {
        $this->materialService->borrarModelo($id);
        return response()->json()
            ->setStatusCode(204);
    }

    public function obtenerTodos()
    {
        return MaterialResource::collection($this->materialService->obtenerTodos());
    }

    public function obtenerPorId(string $id)
    {
        return MaterialResource::make($this->materialService->obtenerPorId($id));
    }
}
