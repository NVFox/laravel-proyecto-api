<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepartamentoResource;
use App\Services\departamento\IDepartamentoService;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    public function __construct(
        protected IDepartamentoService $departamentoService
    ) {}

    public function obtenerTodos()
    {
        return DepartamentoResource::collection($this->departamentoService
            ->obtenerTodos());
    }
}
