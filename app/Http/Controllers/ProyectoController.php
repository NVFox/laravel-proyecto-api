<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProyectoRequest;
use App\Http\Requests\UpdateProyectoRequest;
use App\Http\Resources\ProyectoResource;
use App\Services\proyecto\IProyectoService;

class ProyectoController extends Controller
{
    public function __construct(
        protected IProyectoService $proyectoService
    ) {}

    public function crearModelo(StoreProyectoRequest $request)
    {
        return ProyectoResource::make($this->proyectoService->crearModelo($request->validated()))
            ->response()
                ->setStatusCode(201);
    }

    public function actualizarModelo(string $id, UpdateProyectoRequest $request)
    {
        $this->proyectoService->actualizarModelo($id, $request->validated());
        return response()->json()
            ->setStatusCode(204);
    }

    public function borrarModelo(string $id) 
    {
        $this->proyectoService->borrarModelo($id);
        return response()->json()
            ->setStatusCode(204);
    }

    public function obtenerTodos()
    {
        return ProyectoResource::collection($this->proyectoService->obtenerTodos());
    }

    public function obtenerPorId(string $id)
    {
        return ProyectoResource::make($this->proyectoService->obtenerPorId($id));
    }
}
