<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMaterialProyectoRequest;
use App\Http\Resources\MaterialResource;
use App\Services\proyecto\material\IMaterialProyectoService;
use Illuminate\Http\Request;

class MaterialProyectoController extends Controller
{
    public function __construct(
        protected IMaterialProyectoService $materialProyectoService
    ) {}

    public function agregarMaterialesAProyecto(string $id, StoreMaterialProyectoRequest $request) {
        return MaterialResource::collection($this->materialProyectoService
            ->agregarMaterialesAProyecto($id, $request->validated()["materiales"]))
                ->response()
                    ->setStatusCode(201);
    }

    public function actualizarMaterialesDeProyecto(string $id, StoreMaterialProyectoRequest $request) {
        $this->materialProyectoService
            ->borrarMaterialesDeProyecto($id, $request->validated()["materiales"]);
        return response()->json()
            ->setStatusCode(204);
    }

    public function obtenerMaterialesDeProyecto(string $id) {
        return MaterialResource::collection($this->materialProyectoService
            ->obtenerMaterialesDeProyecto($id));
    }
}
