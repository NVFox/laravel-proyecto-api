<?php

namespace App\Http\Controllers;

use App\Http\Resources\MedidaResource;
use App\Services\medida\IMedidaService;
use Illuminate\Http\Request;

class MedidaController extends Controller
{
    public function __construct(
        protected IMedidaService $medidaService
    ) {}

    public function obtenerTodos()
    {
        return MedidaResource::collection($this->medidaService
            ->obtenerTodos());
    }
}
