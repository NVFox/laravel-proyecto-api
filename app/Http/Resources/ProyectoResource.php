<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProyectoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $ciudad = $this->whenLoaded("ciudad")
            ->load("departamento");

        $departamento = $ciudad->departamento;

        return [
            "id" => $this->id,
            "nombre" => $this->nombre,
            "departamento" => DepartamentoResource::make($departamento),
            "ciudad" => CiudadResource::make($ciudad)
        ];
    }
}
