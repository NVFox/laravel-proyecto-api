<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReporteMaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $materiales = $this->materiales;
        $precioTotal = 0;

        foreach ($materiales as $material) {
            $precioTotal += $material->precio;
        }

        return [
            "proyecto" => ProyectoResource::make($this),
            "materiales" => MaterialResource::collection($materiales->load("medida")),
            "total" => $precioTotal
        ];
    }
}
