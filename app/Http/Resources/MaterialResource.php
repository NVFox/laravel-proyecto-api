<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $medida = $this->whenLoaded("medida");

        return [
            "id" => $this->id,
            "codigo" => $this->codigo,
            "medida" => $medida->nombre,
            "unidad" => $this->unidad,
            "divisa" => $this->divisa,
            "precio" => $this->precio,
            "descripcion" => $this->descripcion
        ];
    }
}
