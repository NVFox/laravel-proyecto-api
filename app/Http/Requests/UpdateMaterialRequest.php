<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "codigo" => "nullable|string|max:255",
            "medida" => "nullable|string|max:36",
            "unidad" => "nullable|integer",
            "divisa" => "nullable|string|max:5",
            "precio" => "nullable|integer",
            "descripcion" => "nullable|string"
        ];
    }
}
