<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    use HasFactory, HasUuids;

    protected $table = "ciudades";
    protected $keyType = "uuid";
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ["nombre"];

    public function departamento() {
        return $this->belongsTo(Departamento::class);
    }
}
