<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory, HasUuids;

    protected $keyType = "uuid";
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ["nombre"];

    public function materiales() 
    {
        return $this->belongsToMany(Material::class, "materiales_por_proyecto", 
            "proyecto_id", "material_id");
    }

    public function ciudad() 
    {
        return $this->belongsTo(Ciudad::class);
    }
}
