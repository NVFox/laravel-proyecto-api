<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory, HasUuids;

    protected $table = "materiales";
    protected $keyType = "uuid";
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ["codigo", "unidad", "divisa", 
        "precio", "descripcion"];

    public function medida() 
    {
        return $this->belongsTo(Medida::class, "medida_unidad");
    }
}
