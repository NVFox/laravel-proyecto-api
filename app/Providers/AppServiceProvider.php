<?php

namespace App\Providers;

use App\Services\ciudad\CiudadService;
use App\Services\ciudad\ICiudadService;
use App\Services\departamento\DepartamentoService;
use App\Services\departamento\IDepartamentoService;
use App\Services\material\IMaterialService;
use App\Services\material\MaterialService;
use App\Services\medida\IMedidaService;
use App\Services\medida\MedidaService;
use App\Services\proyecto\IProyectoService;
use App\Services\proyecto\material\IMaterialProyectoService;
use App\Services\proyecto\material\MaterialProyectoService;
use App\Services\proyecto\ProyectoService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(IProyectoService::class, ProyectoService::class);
        $this->app->bind(IMaterialService::class, MaterialService::class);
        $this->app->bind(IMaterialProyectoService::class, MaterialProyectoService::class);

        $this->app->bind(ICiudadService::class, CiudadService::class);
        $this->app->bind(IDepartamentoService::class, DepartamentoService::class);
        $this->app->bind(IMedidaService::class, MedidaService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
