<?php

namespace App\Services\departamento;

use App\Exceptions\NotFoundException;
use App\Models\Departamento;
use App\Services\departamento\IDepartamentoService;

class DepartamentoService implements IDepartamentoService 
{
    public function obtenerTodos() 
    {
        return Departamento::all();
    }

    public function obtenerPorId(string $id) 
    {
        return Departamento::find($id)
            ?? throw new NotFoundException("departamento");
    }
}

?>