<?php

namespace App\Services\shared\validators\factories;

use App\Models\Material;
use App\Services\material\validators\MaterialResourceValidator;
use App\Services\shared\validators\IResourceValidator;

class ResourceValidatorFactory 
{
    private array $validators;

    public function __construct() {
        $this->validators = [
            Material::class => new MaterialResourceValidator()
        ];
    }

    public function get(string $name): IResourceValidator {
        return $this->validators[$name];
    }
}

?>