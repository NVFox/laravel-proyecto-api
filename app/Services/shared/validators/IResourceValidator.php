<?php

namespace App\Services\shared\validators;

interface IResourceValidator 
{
    public function validate($modelo, $input): void;
}

?>