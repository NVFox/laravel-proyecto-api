<?php

namespace App\Services\medida;

use App\Exceptions\NotFoundException;
use App\Models\Medida;

class MedidaService implements IMedidaService
{
    public function obtenerTodos()
    {
        return Medida::all();
    }

    public function obtenerPorId(string $id)
    {
        return Medida::find($id) 
            ?? throw new NotFoundException("medida");
    }
}

?>