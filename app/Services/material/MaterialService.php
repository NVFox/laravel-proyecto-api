<?php

namespace App\Services\material;

use App\Exceptions\NotFoundException;
use App\Models\Material;
use App\Models\Proyecto;
use App\Services\medida\IMedidaService;
use App\Services\shared\validators\factories\ResourceValidatorFactory;
use App\Services\shared\validators\IResourceValidator;
use Illuminate\Database\Eloquent\Collection;

class MaterialService implements IMaterialService
{
    protected IResourceValidator $resourceValidator;

    public function __construct(
        protected IMedidaService $medidaService
    ) {
        $factory =  new ResourceValidatorFactory();
        $this->resourceValidator = $factory->get(Material::class);
    }

    public function obtenerReportePorProyecto(string $id) {
        return Proyecto::with(["materiales", "ciudad"])
            ->where("id", $id)
            ->firstOr(function() {
                throw new NotFoundException("proyecto");
            });
    }

    public function crearModelo($modelo) 
    {
        $material = new Material();
        $material->divisa = "COP";

        $medida = $this->medidaService
            ->obtenerPorId($modelo["medida"]);

        $material->medida()->associate($medida);
        $this->resourceValidator->validate($material, $modelo);

        $material->save();
        return $material;
    }

    public function actualizarModelo(string $id, $modelo) 
    {
        $material = Material::find($id);
        $material 
            ?? throw new NotFoundException("material");

        if (isset($modelo["medida"])) {
            $medida = $this->medidaService
                ->obtenerPorId($modelo["medida"]);

            $material->medida()->associate($medida);
        }

        $this->resourceValidator->validate($material, $modelo);

        $material->save();
    }

    public function borrarModelo(string $id) 
    {
        $material = Material::find($id);
        $material 
            ? $material->delete() 
            : throw new NotFoundException("material");
    }

    public function obtenerTodos()
    {
        return Material::all()->load("medida");
    }

    public function obtenerPorId(string $id)
    {
        $material = Material::find($id);
        return $material 
            ? $material->load("medida") 
            : throw new NotFoundException("material");
    }
}

?>