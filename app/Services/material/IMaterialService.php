<?php

namespace App\Services\material;

use App\Services\crud\ICrudService;

interface IMaterialService extends ICrudService
{
    public function obtenerReportePorProyecto(string $id);
}

?>