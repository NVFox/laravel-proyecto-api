<?php

namespace App\Services\material\validators;

use App\Exceptions\AlreadyExistsException;
use App\Models\Material;
use App\Services\shared\validators\IResourceValidator;

class MaterialResourceValidator implements IResourceValidator
{
    protected array $definitions;

    public function __construct() 
    {
        $this->definitions = [
            "codigo" => function (Material $material, $data) {
                $codigo = Material::where('codigo', $data)
                    ->first();

                if ($codigo) throw new AlreadyExistsException("codigo");

                $material->codigo = $data;
            },
            "unidad" => function (Material $material, $data) {
                $material->unidad = $data;
            }, 
            "divisa" => function (Material $material, $data) {
                $material->divisa = $data;
            }, 
            "precio" => function (Material $material, $data) {
                $material->precio = $data;
            }, 
            "descripcion" => function (Material $material, $data) {
                $material->descripcion = $data;
            }
        ];
    }

    public function validate($modelo, $input): void {
        foreach ($input as $key => $value) {
            if(isset($value) && isset($this->definitions[$key])) {
                $this->definitions[$key]($modelo, $value);
            }
        }
    }
}

?>