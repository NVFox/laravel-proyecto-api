<?php

namespace App\Services\crud;

/**
 * @template T
 */
interface IFindService 
{
    /**
     * @return array
     */
    public function obtenerTodos();

    /**
     * @param string $id
     * @return T
     */
    public function obtenerPorId(string $id);
}

?>