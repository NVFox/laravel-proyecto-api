<?php

namespace App\Services\crud;

/**
 * @template T
 */
interface ICrudService extends IFindService
{
    /**
     * @param T $modelo
     * @return T
     */
    public function crearModelo($modelo);

    /**
     * @param string $id
     * @param T $modelo
     */
    public function actualizarModelo(string $id, $modelo);

    /**
     * @param T $modelo
     */
    public function borrarModelo(string $id);
}

?>