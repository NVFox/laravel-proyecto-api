<?php

namespace App\Services\proyecto\material;

use App\Models\Proyecto;
use Illuminate\Database\Eloquent\Collection;

interface IMaterialProyectoService
{
    public function agregarMaterialesAProyecto(string $id, array $materiales): Collection;

    public function borrarMaterialesDeProyecto(string $id, array $materiales): void;

    public function obtenerMaterialesDeProyecto(string $id): Collection;
}

?>