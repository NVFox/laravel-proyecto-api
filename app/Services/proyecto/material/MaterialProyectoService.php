<?php

namespace App\Services\proyecto\material;

use App\Exceptions\NotFoundException;
use App\Models\Material;
use App\Models\Proyecto;
use App\Services\material\IMaterialService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class MaterialProyectoService implements IMaterialProyectoService
{

    public function __construct(
        protected IMaterialService $materialService
    ) {}

    public function agregarMaterialesAProyecto(string $id, array $materiales): Collection {
        $proyecto = Proyecto::find($id);
        $proyecto
            ?? throw new NotFoundException("proyecto");

        $materialesDeProyecto = Material::whereIn("id", $materiales)->get();
        $proyecto->materiales()->attach($materialesDeProyecto);

        return $materialesDeProyecto->load("medida");
    }

    public function borrarMaterialesDeProyecto(string $id, array $materiales): void {
        DB::table("materiales_por_proyecto")
            ->where("proyecto_id", $id)
            ->whereIn("material_id", $materiales)
            ->delete();
    }

    public function obtenerMaterialesDeProyecto(string $id): Collection {
        $proyecto = Proyecto::find($id);
        $proyecto
            ?? throw new NotFoundException("proyecto");

        return $proyecto->materiales()
            ->get()
            ->load("medida");
    }
}

?>