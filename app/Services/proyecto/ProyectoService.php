<?php

namespace App\Services\proyecto;

use App\Exceptions\NotFoundException;
use App\Models\Proyecto;
use App\Services\ciudad\ICiudadService;
use App\Services\proyecto\IProyectoService;

class ProyectoService implements IProyectoService
{

    public function __construct(
        protected ICiudadService $ciudadService
    ) {}

    public function crearModelo($modelo) 
    {
        $proyecto = new Proyecto();
        $ciudad = $this->ciudadService
            ->obtenerPorId($modelo["ciudad"]);

        $proyecto->nombre = $modelo["nombre"];
        $proyecto->ciudad()->associate($ciudad);

        $proyecto->save();
        return $proyecto;
    }

    public function actualizarModelo(string $id, $modelo) 
    {
        $proyecto = Proyecto::find($id);

        if (!$proyecto) 
        {
            throw new NotFoundException("proyecto");
        }

        if (isset($modelo["ciudad"])) {
            $ciudad = $this->ciudadService
            ->obtenerPorId($modelo["ciudad"]);

            $proyecto->ciudad()->associate($ciudad);
        }

        if (isset($modelo["nombre"])) {
            $proyecto->nombre = $modelo["nombre"];
        }

        $proyecto->save();
    }

    public function borrarModelo(string $id) 
    {
        $proyecto = Proyecto::find($id);
        $proyecto 
            ? $proyecto->delete() 
            : throw new NotFoundException("proyecto");
    }

    public function obtenerTodos()
    {
        return Proyecto::all()->load("ciudad");
    }

    public function obtenerPorId(string $id)
    {
        $proyecto = Proyecto::find($id);
        return $proyecto 
            ? $proyecto->load("ciudad") 
            : throw new NotFoundException("proyecto");
    }
}

?>