<?php

namespace App\Services\ciudad;

use App\Exceptions\NotFoundException;
use App\Models\Ciudad;
use App\Models\Departamento;
use App\Services\ciudad\ICiudadService;

class CiudadService implements ICiudadService
{
    public function obtenerTodos()
    {
        return Ciudad::all();
    }

    public function obtenerPorId(string $id)
    {
        return Ciudad::find($id) 
            ?? throw new NotFoundException("ciudad");
    }

    public function encontrarTodosPorDepartamento($departamento) 
    {
        return Ciudad::where("departamento_id", $departamento)
            ->get();
    }
}

?>