<?php

namespace App\Services\ciudad;

use App\Services\crud\IFindService;

interface ICiudadService extends IFindService
{
    public function encontrarTodosPorDepartamento($departamento);
}

?>