<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as Request;

class NotFoundException extends Exception
{
    public function __construct(
        private string $type
    ) {}

    public function render(Request $request): JsonResponse
    {
        return response()->json([
            "message" => "No se ha encontrado el registro de ".$this->type."."
        ], 404);
    }
}
