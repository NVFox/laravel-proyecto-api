<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AlreadyExistsException extends Exception
{
    public function __construct(
        private string $type
    ) {}

    public function render(Request $request): JsonResponse
    {
        return response()->json([
            "message" => $this->type." ya existe."
        ], 400);
    }
}
