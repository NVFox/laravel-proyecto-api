<?php

namespace Database\Seeders;

use App\Models\Ciudad;
use App\Models\Departamento;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $response = Http::get("https://raw.githubusercontent.com/marcovega/colombia-json/master/colombia.min.json");
        foreach ($response->json() as $item) {
            $nombreDepartamento = $item["departamento"];
            $departamento = $this->createDepartamento($nombreDepartamento);

            foreach ($item["ciudades"] as $ciudad) {
                $this->createCiudad($ciudad, $departamento);
            }
        }
    }

    private function createDepartamento(string $nombre): Departamento {
        $departamento = new Departamento();
        $departamento->nombre = $nombre;
        $departamento->save();

        return $departamento;
    }

    private function createCiudad(string $nombre, Departamento $departamento): void {
        $ciudad = new Ciudad();
        $ciudad->nombre = $nombre;
        $ciudad->departamento()->associate($departamento);
        $ciudad->save();
    }
}
