<?php

namespace Database\Seeders;

use App\Models\Medida;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $medidas = ["kg", "m2", "und"];

        foreach ($medidas as $nombre) {
            $medida = new Medida();
            $medida->nombre = $nombre;
            $medida->save();
        }
    }
}
