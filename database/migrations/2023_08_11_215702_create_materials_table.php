<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materiales', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("codigo")->unique();
            $table->foreignUuid("medida_unidad")->references("id")->on("medidas")
                ->onUpdate("cascade")
                ->onDelete("cascade");
            $table->integer("unidad");
            $table->string("divisa", 5);
            $table->integer("precio");
            $table->text("descripcion");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materiales');
    }
};
