<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materiales_por_proyecto', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid("proyecto_id")->references("id")->on("proyectos")
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreignUuid("material_id")->references("id")->on("materiales")
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materiales_por_proyecto');
    }
};
